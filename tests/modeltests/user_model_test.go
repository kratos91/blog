package modeltests

import (
	"blog/api/models"
	"gopkg.in/go-playground/assert.v1"
	"log"
	"testing"
)

func TestFindAllUsers(t *testing.T) {
	err:=refreshUserTable()
	if err != nil {
		log.Fatal(err)
	}
	err=seedUsers()
	if err != nil {
		log.Fatal(err)
	}

	users,err:=userInstance.FindAllUsers(server.DB)
	if err != nil {
		t.Errorf("Error:this is the error getting the users: %v\n", err)
		return
	}
	assert.Equal(t,len(*users),2)
}

func TestSaveUser(t *testing.T) {
	err:=refreshUserTable()
	if err != nil {
		log.Fatal(err)
	}
	newUser:=models.User{
		ID:1,
		Email: "test@gmail.com",
		NickName: "test",
		Password: "password",
	}

	savedUser,err:=newUser.SaveUser(server.DB)
	if err != nil {
		t.Errorf("Error:this is the error getting the users: %v\n",err)
	}
	assert.Equal(t,newUser.ID,savedUser.ID)
	assert.Equal(t,newUser.Email,savedUser.Email)
	assert.Equal(t,newUser.NickName,savedUser.NickName)
}

func TestUpdateAUser(t *testing.T) {
	err:=refreshUserTable()
	if err != nil {
		log.Fatal(err)
	}

	user,err:=seedOneUser()
	if err != nil {
		log.Fatalf("Cannot seed user: %v\n",err)
	}

	userUpdate:=models.User{
		ID:1,
		NickName: "modUpdate",
		Email: "modUpdate@gmail.com",
		Password: "password",
	}
	updateUser,err:=userUpdate.UpdateAUser(server.DB,user.ID)
	if err != nil {
		t.Errorf("Error:this is the error updating the user: %v\n",err)
	}
	assert.Equal(t,updateUser.ID,userUpdate.ID)
	assert.Equal(t,updateUser.Email,userUpdate.Email)
	assert.Equal(t,updateUser.NickName,userUpdate.NickName)
}

func TestDeleteUser(t *testing.T) {
	err:=refreshUserTable()
	if err != nil {
		log.Fatal(err)
	}

	user,err:=seedOneUser()
	if err != nil {
		log.Fatalf("Error:Cannot seed user: %v\n",err)
	}

	isDeleted,err:=userInstance.DeleteAUser(server.DB,user.ID)
	if err != nil {
		t.Errorf("Error:this is the error deleting the user: %v\n",err)
		return
	}

	assert.Equal(t,isDeleted,int64(1))
}
