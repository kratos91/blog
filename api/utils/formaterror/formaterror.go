package formaterror

import (
	"errors"
	"strings"
)

func FormatError(err string) error {

	if strings.Contains(err, "nickname") {
		return errors.New("Error:Nickname Already Taken")
	}

	if strings.Contains(err, "email") {
		return errors.New("Error:Email Already Taken")
	}

	if strings.Contains(err, "title") {
		return errors.New("Error:Title Already Taken")
	}
	if strings.Contains(err, "hashedPassword") {
		return errors.New("Error:Incorrect Password")
	}
	return errors.New("Error:Incorrect Details")
}